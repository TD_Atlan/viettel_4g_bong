*** Settings ***
Resource          ../GLOBAL/Pages.robot

*** Test Cases ***
TC00_Register
    Register Random Account

TC01_HomePage
    [Setup]    Go To HomePage
    Check HomePage
    [Teardown]

TC02_Sports
    [Setup]    Go To Home Page
    Check Sports
    [Teardown]

TC03_Virtual_Sport
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Sport
    [Teardown]

TC04_Virtual_Casino
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Casino
    [Teardown]

TC04.1_Virtual_Casino_OneRNG
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Casino OneRNG
    [Teardown]

TC04.2_Virtual_Casino_Pragmatic_Play
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Casino Pragmatic Play
    [Teardown]

TC04.3_Virtual_Casino_Spadegaming
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Casino Spadegaming
    [Teardown]

TC04.4_Virtual_Casino_Voidbridge
    [Setup]    Go To Home Page
    [Template]
    Go To Virtual Casino Voidbridge
    [Teardown]

TC05_NumberGame
    [Setup]    Go To Home Page
    [Template]
    Check NumberGame
    [Teardown]

TC06_Keno
    [Setup]    Go To Home Page
    Check Keno
    [Teardown]

TC07_Lottery
    [Setup]    Go To Home Page
    Check Lottery
    [Teardown]

TC08_TP_InGame
    [Setup]    Go To Home Page
    Check TP InGame
    [Teardown]

TC09_TP_NoHu
    [Setup]    Go To Home Page
    Check TP NoHu
    [Teardown]

TC10_TP_FishGame
    [Setup]    Go To Home Page
    [Template]
    Check TP FishGame
    [Teardown]

TC11_TP_Lode
    [Setup]    Go To Home Page
    Check TP Lode
    [Teardown]

TC12_TP_Dial
    [Setup]    Go To Home Page
    [Template]
    Check TP Dial
    [Teardown]

TC13_TP_Keno
    [Setup]    Go To Home Page
    Check TP Keno
    [Teardown]

TC14_TP_NumberGame
    [Setup]    Go To Home Page
    Check TP NumberGame
    [Teardown]

TC15_Casino_AllBet
    [Setup]    Go To Home Page
    Check Casino AllBet
    [Teardown]

TC16_Casino_Vivo
    [Setup]    Go To Home Page
    Check Casino Vivo
    [Teardown]

TC17_Casino_Ezugi
    [Setup]    Go To Home Page
    Check Casino Ezugi
    [Teardown]

TC18_Casino_Ebet
    [Setup]    Go To Home Page
    Check Casino Ebet
    [Teardown]

*** Keywords ***
