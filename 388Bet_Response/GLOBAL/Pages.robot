*** Settings ***
Library           RPA.Browser    run_on_failure=Nothing    timeout=30    implicit_wait=10
Resource          Locators.robot
Resource          Template.robot
Library           DateTime
Library           String
Library           OperatingSystem
Library           RPA.HTTP

*** Keywords ***
Go To Home Page
    Open Browser    ${HOMEPAGE URL}    headlesschrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080
    Maximize Browser Window
    Login With Valid Data
    Wait For Condition    return document.readyState=='complete'
    [Teardown]

Append Content To File
    [Arguments]    ${content}
    Append To File    ${path}    ${content}    encoding=UTF-8

Run Keyword When A Step Failed
    [Arguments]    ${content}
    Run Keyword If    '${KEYWORD STATUS}' == 'FAIL'    Run Keywords    Append Content To File    FAILED\n${content}
    ...    AND    Fail
    Close All Browsers

Login With Valid Data
    Input Text When Element Is Visible    css:input#user    datttit01@yandex.com
    Input Text When Element Is Visible    css:input#password    123456
    Click Element If Visible    css:a#btnlogin
    Sleep    3
    [Teardown]

Check_Response_For_Non_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    o88userApp.verifyPlayGame    OK    (    )    '    '    ,    ${SPACE}    //    https:    http:
        ${base_url}    Evaluate    "//" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        Run Keyword If    ${base_url}==True    Create Session    18    https://${uri}    retry_method_list=['GET']    disable_warnings=disable_warnings    timeout=5
        ${resp}    Get Request    18    ${EMPTY}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${resp}    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check Helps
    Click Element If Visible    css:nav [href='https\:\/\/388bet\.us\/Asia\/vn\/tro-giup\/']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@src]    src
    Check_Response    //div[@class='page-wrapper']//a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    NumberGame\nOh! đã xảy ra lỗi\r\n

Check Games
    Click Element If Visible    css:[href='https\:\/\/388bet\.us\/Asia\/vn\/tro-choi\.html']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@src]    src
    Check_Response    //div[@class='listAllGameDesktop']//a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    Games\nOh! đã xảy ra lỗi\r\n

Check Casino
    Click Element If Visible    css:nav [href='https\:\/\/388bet\.us\/Asia\/vn\/song-bai\.html']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class='gameCasinoDesktop']/div[*]/div[@class='info']/a[@href]    href
    Check_Response    //img[@src]    src
    [Teardown]    Run Keyword When A Step Failed    Casino\nOh! đã xảy ra lỗi\r\n

Check Deposit Page
    Go To    https://388bet.us/Asia/vn/tro-giup/demo-account-36.html
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@src]    src
    Check_Response_For_Https    //a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    Deposit Page\nOh! đã xảy ra lỗi\r\n

Check HomePage
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Https    //a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    HomePage\nOh! đã xảy ra lỗi\r\n

Check Sports
    Click Element If Visible    css:nav [href='https\:\/\/388bet\.us\/Asia\/vn\/ty-le-keo\.html']
    Wait For Condition    return document.readyState=='complete'
    Wait Until Page Contains Element    css:div:nth-of-type(1) > div:nth-of-type(2) > .league > .leagueName
    Check_Response    //img[@src]    src
    Check_Response_For_Https    //a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    Sports\nOh! đã xảy ra lỗi\r\n

Check_Response_For_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        Log    ${attribute}
        ${uri}    Remove String    ${attribute}    javascript:void(0)    https://388bet.us/Asia/vn/thanh-vien.html#3    https://388bet.us/Asia/vn/thanh-vien.html#2    https://388bet.us/Asia/vn/thanh-vien.html#1    https://388bet.us/Asia/vn/thanh-vien.html    tel:0962383838    mailto:support.vn@388bet.com    https://www.388bet.us/Asia/vn/soi-keo.html
        ${check_url}    Evaluate    '${uri}' is not '${EMPTY}'
        ${stat}    Run Keyword If    ${check_url}==False    Run Keyword    Http Get    ${HOMEPAGE URL}
        ...    ELSE    Http Get    ${uri}
        Run Keyword If    ${stat.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${stat.status_code} - Reason = ${stat.reason}\nElapsed Time = ${stat.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Register Random Account
    Open Browser    ${HOMEPAGE URL}    headlesschrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080
    Create File    ${path}
    ${getemail}    Get Current Date    result_format=%b%d%H%M%S@yandex.com
    Convert To String    ${getemail}
    Set Global Variable    ${getemail}
    Input Text When Element Is Visible    css:input#txt-email    ${getemail}
    Input Text When Element Is Visible    css:input#txt-password    123456
    Input Text When Element Is Visible    css:input#txt-phone    0123456789
    Click Element If Visible    css:.icon-next-resgister
    Sleep    5s
    Wait Until Page Contains Element    css:.user-email
    Element Should Contain    css:.user-email    ${getemail}
    [Teardown]    Run Keyword When A Step Failed    Register\nOh! đã xảy ra lỗi\r\n
