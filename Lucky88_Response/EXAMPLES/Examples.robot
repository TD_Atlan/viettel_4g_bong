*** Settings ***
Resource          ../GLOBAL/Pages.robot

*** Variables ***
${Content_type}    ${EMPTY}

*** Test Cases ***
demo
    demo4

Check_API_Links
    [Setup]
    [Template]
    demo4

*** Keywords ***
Go To Casino Ebet Page
    [Arguments]    ${section_name}    ${sub}
    Wait For Condition    return document.readyState=='complete'
    Wait Until Page Contains Element    css:a[title='Sòng bài trực tuyến']
    Scroll Element Into View    css:a[title='Sòng bài trực tuyến']
    Click Element    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Click Element    css:div#lcEbet > div > div:nth-of-type(4) > .casino-live__link
    Switch Window    NEW
    Start Counting Response Time
    Wait For Condition    return document.readyState=='complete'
    ${status_ele}    Run Keyword And Return Status    Wait Until Page Contains Element    css:div#gameMainDiv > canvas    ${TIMEOUT}
    ${stop}    Get Current Date
    ${diff}    Subtract Date From Date    ${stop}    ${start}
    ${url}    Get Location
    Run Keyword If    ${status_ele}==False    Append Content To File    ${section_name}\n${sub}\n${FAILED non timeout}\n${url}\nResponse Time: ${diff}\r\n
    [Teardown]    Run Keyword When A Step Failed    ${section_name}\n${sub}\n${FAILED timeout}\r\n

Go To Casino Eguzi Page
    [Arguments]    ${section_name}    ${sub}
    Wait For Condition    return document.readyState=='complete'
    Wait Until Page Contains Element    css:a[title='Sòng bài trực tuyến']
    Scroll Element Into View    css:a[title='Sòng bài trực tuyến']
    Click Element    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Click Element    css:div#lcEzugi > div > div:nth-of-type(4) > .casino-live__link
    Switch Window    NEW
    Start Counting Response Time
    Wait For Condition    return document.readyState=='complete'
    ${status_ele}    Run Keyword And Return Status    Wait Until Page Contains Element    css:div:nth-of-type(1) > .table__black___1Vjom.table__table___2k7-q .table__table_image___1eeWa    ${TIMEOUT}
    ${stop}    Get Current Date
    ${diff}    Subtract Date From Date    ${stop}    ${start}
    ${url}    Get Location
    Run Keyword If    ${status_ele}==False    Append Content To File    ${section_name}\n${sub}\n${FAILED non timeout}\n${url}\nResponse Time: ${diff}\r\n
    [Teardown]    Run Keyword When A Step Failed    ${section_name}\n${sub}\n${FAILED timeout}\r\n

demo1
    Open Browser    ${EMPTY}    ${BROWSER}
    Create Session    588    http://five88.one/    retry_method_list=['GET']
    ${status}    Get Request    588    style.css&v=368
    Status Should Be    200    ${status}

Open Browser To Check API
    Open Browser    https://lucky88.win/casino.aspx    headlesschrome

Run Keyword When A Step Failed For Check API
    [Arguments]    ${content}
    Run Keyword If    '${KEYWORD STATUS}' == 'FAIL'    Run Keywords    Append To File    D:\\PythonAutomation\\PROJECT_BONG\\Check_API\\RESULTS\\report_api.txt    ${content}    encoding=UTF-8
    ...    AND    Fail

Check_API
    ${data}    Get File    D:\\Demo\\Auto\\Lucky88\\DATA\\api_link.txt
    @{api_link}    Split String    ${data}
    Create File    D:\\PythonAutomation\\PROJECT_BONG\\Check_API\\RESULTS\\report_api.txt
    FOR    ${link}    IN    @{api_link}
        Create Session    Check API    ${link}    disable_warnings=disable_warnings    timeout=5
        ${sta_resp}    Run Keyword And Return Status    My Response
        Run Keyword If    ${sta_resp}==True    Log To Console    PASSED
        Run Keyword If    ${sta_resp}==False    Log To Console    FAILED
    END
    [Teardown]

My Response
    ${response}    Get Request    Check API    /
    Set Global Variable    ${response}

demo4
    Open Browser    https://lucky88.win/game.aspx    chrome
    Login With Valid Data
    Go To    https://lucky88.win/game.aspx
    Wait Until Keyword Succeeds    3x    5s    Select Frame    css:iframe[src='https://lucky88.win/game/load.aspx']
    Wait Until Keyword Succeeds    3x    5s    Select Frame    css:#frmGame
    ${count_ele}    Get WebElements    //img[@src]
    Log    ${count_ele}
    @{auth}    Create List    datttit04    123456
    FOR    ${x}    IN    @{count_ele}
        ${link_img}    Get Element Attribute    ${x}    src
        Log To Console    ${link_img}
        Comment    ${uri}    Remove String    ${link_img}    ${link_img}
        Create Session    mayman    ${link_img}    retry_method_list=['GET']    disable_warnings=disable_warnings    timeout=5    auth=${auth}
        ${status}    Get Request    mayman    ${EMPTY}
        Log To Console    ${status}
        Run Keyword If    ${status.status_code}==200    Log To Console    PASSED
        ...    ELSE    Log To Console    FAILED
    END
    [Teardown]    Close All Browsers

demo6
    Open Browser    ${EMPTY}    chrome
    Go To    https://lucky88.win/lobby.aspx?game=slot
    Login With Valid Data
    Go To    https://lucky88.win/lobby.aspx?game=slot
    ${xpath_list}    Set Variable    //a[@href][@class="games__link"]    #//a[@href][@class="casino-live__link"]
    ${count_ele}    Get WebElements    ${xpath_list}
    Log To Console    ${count_ele}
    @{auth}    Create List    datttit04    123456
    FOR    ${x}    IN    @{count_ele}
        ${link_img}    Get Element Attribute    ${x}    href
        Log To Console    ${link_img}
        ${uri}    Remove String    ${link_img}    ${link_img}
        Create Session    mayman    ${link_img}    retry_method_list=['GET']    disable_warnings=disable_warnings    timeout=5    auth=${auth}
        ${status}    Get Request    mayman    ${uri}
        Log To Console    ${status}
        Run Keyword If    ${status.status_code}==200    Log To Console    PASSED
        ...    ELSE    Log To Console    FAILED
    END
    [Teardown]    Close All Browsers

Demo5
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    javascript:void(0)    \    \    \    https://388bet.top/Asia/vn/thanh-vien.html#1
        ${uri1}    Remove String    ${uri}    https://388bet.top/Asia/vn/thanh-vien.html
        ${uri2}    Remove String    ${uri1}    https://388bet.top/Asia/vn/thanh-vien.html#1
        ${uri3}    Remove String    ${uri2}    https://388bet.top/Asia/vn/thanh-vien.html#2
        ${uri4}    Remove String    ${uri3}    tel:0962383838
        ${uri5}    Remove String    ${uri4}    mailto:support.vn@388bet.com
    ${EMPTY}
    ${EMPTY}
    ${EMPTY}
    ${EMPTY}
        ${stat}    Run Keyword And Return Status    Should Not Be Empty    ${uri3}
        Run Keyword If    ${stat}==False    Run Keyword    Create Session    18    https://one88.vn/    retry_method_list=['GET']    disable_warnings=disable_warnings    timeout=5
        ...    ELSE    Create Session    18    ${uri3}    retry_method_list=['GET']    disable_warnings=disable_warnings    timeout=5
        ${status}    Get Request    18    ${EMPTY}
        Run Keyword If    ${status.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus Code: ${status.status_code} - Reason: ${status.reason}\r\n
    END
