*** Settings ***
Resource          ../GLOBAL/Pages.robot

*** Test Cases ***
TC00_Register
    [Setup]    My Open Headless Chrom Browser
    Register Random Account

TC01_HomePage
    [Setup]    Go To Home Page
    Check_Response    //img[@src]    src
    [Teardown]    Close All Browsers

TC02_Sports
    [Setup]    Go To Home Page
    Check Sports
    [Teardown]

TC03_Virtual_Sport
    [Setup]    Go To Home Page
    [Template]
    Check Virtual Sport
    [Teardown]

TC04_Games
    [Setup]    Go To Home Page
    [Template]
    Check Games
    [Teardown]

TC04.1_Games_OneRNG
    [Setup]    Go To Home Page
    [Template]
    Check Games OneRNG
    [Teardown]

TC04.2_Games_Pragmatic_Play
    [Setup]    Go To Home Page
    [Template]
    Check Games Pragmatic Play
    [Teardown]

TC04.3_Games_Spadegaming
    [Setup]    Go To Home Page
    [Template]
    Check Games Spadegaming
    [Teardown]

TC04.4_Games_Voidbridge
    [Setup]    Go To Home Page
    [Template]
    Check Games Voidbridge
    [Teardown]

TC05_NumberGame
    [Setup]    Go To Home Page
    [Template]
    Check NumberGame
    [Teardown]

TC06_Keno
    [Setup]    Go To Home Page
    Check Keno
    [Teardown]

TC07_Lottery
    [Setup]    Go To Home Page
    Check Lottery
    [Teardown]

TC08_Table_Games
    [Setup]    Go To Home Page
    Check Table Games
    [Teardown]

TC09_Virtual_Games
    [Setup]    Go To Home Page
    Check Virtual Games
    [Teardown]

TC10_Ban_Ca_Hoang_Gia
    [Setup]    Go To Home Page
    Check Ban Ca Hoang Gia
    [Teardown]

TC11_GG_Number_Game
    [Setup]    Go To Home Page
    Check GG NumberGame
    [Teardown]

TC12_GG_Keno
    [Setup]    Go To Home Page
    [Template]
    Check GG Keno
    [Teardown]

TC13_GG_NoHu_QuaySo_InGame
    [Setup]    Go To Home Page
    Check GG NoHu QuaySo InGame
    [Teardown]

TC14_GG_Ban_Ca
    [Setup]    Go To Home Page
    [Template]
    Check GG BanCa
    [Teardown]

TC15_GG_Lottery
    [Setup]    Go To Home Page
    Check GG Lottery
    [Teardown]

TC18_Casino_AllBet
    [Setup]    Go To Home Page
    Check Casino AllBet
    [Teardown]

TC18_Casino_Vivo
    [Setup]    Go To Home Page
    Check Casino Vivo
    [Teardown]

TC19_Casino_Ezugi
    [Setup]    Go To Home Page
    Check Casino Ezugi
    [Teardown]

TC20_Casino_Ebet
    [Setup]    Go To Home Page
    Check Casino Ebet
    [Teardown]

TC21_Esport
    [Setup]    Go To Home Page
    Check Esport
    [Teardown]

TC22_Saba_Club
    [Setup]    Go To Home Page
    Check Saba Club
    [Teardown]

*** Keywords ***
