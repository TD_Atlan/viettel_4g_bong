*** Settings ***
Library           RPA.Browser    run_on_failure=Nothing    timeout=30    implicit_wait=10
Resource          Locators.robot
Library           DateTime
Library           String
Library           OperatingSystem
Library           RPA.HTTP

*** Keywords ***
Go To Home Page
    My Open Headless Chrome Browser
    Login With Valid Data
    [Teardown]

Append Content To File
    [Arguments]    ${content}
    Append To File    ${path}    ${content}    encoding=UTF-8

Run Keyword When A Step Failed
    [Arguments]    ${content}
    ${url}    Get Location
    Run Keyword If    '${KEYWORD STATUS}' == 'FAIL'    Run Keywords    Append Content To File    FAILED\n${content}
    ...    AND    Fail
    Close All Browsers

Login With Valid Data
    Input Text When Element Is Visible    css:form[name='frm-login-header'] > input[name='username']    ${username}
    Input Text When Element Is Visible    css:form[name='frm-login-header'] > input[name='password']    123123
    Click Element If Visible    css:form[name='frm-login-header'] > .btn.btn-login.text-uppercase.text-white
    Sleep    3
    [Teardown]

Check_Response_For_Non_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    o88userApp.verifyPlayGame    OK    (    )    '    '    ,    ${SPACE}    //    https:    http:
        ${base_url}    Evaluate    "//" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    https://${uri}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Non_Http
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    o88userApp.verifyPlayGame    OK    (    )    '    '    ,    ${SPACE}    //    https:    http:
        ${base_url}    Evaluate    "//" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    http://${uri}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${base_url}    Evaluate    "http" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${resp}    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Games
    Sleep    5s
    FOR    ${x}    IN RANGE    0    4
        Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe[onload='loadingBox()']
        Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
        Sleep    5s
        Execute Javascript    document.getElementsByClassName('fun_mask')[${x}].click();
        Sleep    3s
        @{list}    Get Window Handles
        Switch Window    ${list}[1]
        ${url}    Get Location
        ${status}    Http Get    ${url}
        Run Keyword If    ${status.status_code}!=200    Append Content To File    FAILED\n${url}\nStatus Code: ${status.status_code} - Reason: ${status.reason}\r\n
        Close Window
        Switch Window    MAIN
    END

Check_Response_For_Supplier_Virtual_Casino
    [Arguments]    ${supplier}
    Execute Javascript    document.querySelector("#MG002-all > span").click();
    Execute Javascript    ${supplier}
    Sleep    5s
    Execute Javascript    document.getElementsByClassName('fun_mask')[0].click();
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    ${url}    Get Location
    ${status}    Http Get    ${url}
    Run Keyword If    ${status.status_code}!=200    Append Content To File    FAILED\n${url}\nStatus Code: ${status.status_code} - Reason: ${status.reason}\r\n

Go To Virtual Casino Voidbridge
    Go To    https://one88.me/games/play.aspx
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe[onload='loadingBox()']
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V12").click();
    [Teardown]    Run Keyword When A Step Failed    Virtual Casino - Voidbridge${FAILED msg}

Check Casino
    [Arguments]    ${casino}    ${verify_ele}
    Click Element If Visible    ${casino}
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Wait Until Page Contains Element    ${verify_ele}

Check Casino Ebet
    Click Element If Visible    css:li:nth-of-type(3) > .nav-link.text-uppercase
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Non_Http    //section[@class='live-casino__section live-casino__section--ebet']//div[@class='live-casino__swiper-wrapper swiper-wrapper']//a[@class='live-casino__swiper--link']    onclick
    Check Casino    css:.swiper-container--ebet.swiper-container [data-swiper-slide-index='0']:nth-of-type(4) .live-casino__swiper--img    css:div#gameMainDiv > canvas
    [Teardown]    Run Keyword When A Step Failed    Casino Ebet${FAILED msg}

Check Casino Ezugi
    Click Element If Visible    css:li:nth-of-type(3) > .nav-link.text-uppercase
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Non_Https    //section[@class='live-casino__section live-casino__section--ezugi']//div[@class='live-casino__swiper-wrapper swiper-wrapper']//a[@class='live-casino__swiper--link']    onclick
    Check Casino    css:.swiper-container--ezugi.swiper-container [data-swiper-slide-index='0']:nth-of-type(4) .live-casino__swiper--img    css:.category_page__table_container___2wH_W > div:nth-of-type(1)
    [Teardown]    Run Keyword When A Step Failed    Casino Ezugi${FAILED msg}

Check Casino Vivo
    Click Element If Visible    css:li:nth-of-type(3) > .nav-link.text-uppercase
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Non_Https    //section[@class='live-casino__section live-casino__section--vivo']//div[@class='live-casino__swiper-wrapper swiper-wrapper']//a[@class='live-casino__swiper--link']    onclick
    Check Casino    css:.swiper-container--vivo.swiper-container [data-swiper-slide-index='0']:nth-of-type(4) .live-casino__swiper--img    css:#menu-nav-baccarat
    [Teardown]    Run Keyword When A Step Failed    Casino Vivo${FAILED msg}

Check Casino AllBet
    Click Element If Visible    css:li:nth-of-type(3) > .nav-link.text-uppercase
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Non_Https    //section[@class='live-casino__section live-casino__section--allbet']//a[@class='live-casino__swiper--link']    onclick
    [Teardown]    Run Keyword When A Step Failed    Casino AllBet${FAILED msg}

Check TP NumberGame
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(7) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='numbergametp']/div[@class='games']//a[@href='/game/lobby/numbergame']    href
    [Teardown]    Run Keyword When A Step Failed    TP - NumberGame${FAILED msg}

Check TP Keno
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(6) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='techplay']/div[@class='games']//a[@href='/game/lobby/quayso']    href
    [Teardown]    Run Keyword When A Step Failed    TP - Keno${FAILED msg}

Check TP Dial
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(5) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='techplay']/div[@class='games']//a[@href='/game/lobby/quayso']    href
    [Teardown]    Run Keyword When A Step Failed    TP - Dial${FAILED msg}

Check TP Lode
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(4) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='lotte']/div[@class='games']//a[@href='/game/lobby/lode']    href
    [Teardown]    Run Keyword When A Step Failed    TP - Lode${FAILED msg}

Check TP FishGame
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(3) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='fish']/div[@class='games']//a[@href='/game/lobby/banca']    href
    [Teardown]    Run Keyword When A Step Failed    TP - FishGame${FAILED msg}

Check TP NoHu
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(2) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Non_Https    //a[@class="games__link"][@onclick]    onclick
    [Teardown]    Run Keyword When A Step Failed    TP - NoHu${FAILED msg}

Check TP InGame
    Click Element If Visible    css:.menu-techplay > li:nth-of-type(1) > a
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Https    //*[@class="gamegrid__item"]//a    href
    [Teardown]    Run Keyword When A Step Failed    TP - InGame${FAILED msg}

Check Lottery
    Click Element If Visible    css:li:nth-of-type(7) > .nav-link.text-uppercase
    Click Element If Visible    css:.banner-top.container-layout button
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Select Frame    //iframe[@onload]
    Check_Response    //iframe[@id="frmGame"]    src
    [Teardown]    Run Keyword When A Step Failed    Lottery${FAILED msg}

Check Keno
    Click Element If Visible    css:li:nth-of-type(6) > .nav-link.text-uppercase
    Click Element If Visible    css:.banner-top.container-layout button
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Keno${FAILED msg}

Check NumberGame
    Click Element If Visible    css:li:nth-of-type(5) > .nav-link.text-uppercase
    Click Element If Visible    css:.banner-top.container-layout button
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //iframe[@onload="loadingBox()"]    src
    [Teardown]    Run Keyword When A Step Failed    Number Game${FAILED msg}

Check Virtual Casino Spadegaming
    Go To    https://one88.me/games/play.aspx
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe[onload='loadingBox()']
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V36").click();
    [Teardown]    Run Keyword When A Step Failed    Virtual Casino - Spadegaming${FAILED msg}

Check Virtual Casino Pragmatic Play
    Go To    https://one88.me/games/play.aspx
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe[onload='loadingBox()']
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V32").click();
    [Teardown]    Run Keyword When A Step Failed    Virtual Casino - PragmaticPlay${FAILED msg}

Check Virtual Casino OneRNG
    Go To    https://one88.me/games/play.aspx
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe[onload='loadingBox()']
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V10").click();
    [Teardown]    Run Keyword When A Step Failed    Virtual Casino - OneRNG${FAILED msg}

Check Virtual Casino
    Go To    https://one88.me/games/play.aspx
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Games
    [Teardown]    Run Keyword When A Step Failed    Virtual Casino${FAILED msg}

Check Virtual Sport
    Click Element If Visible    css:li:nth-of-type(2) > .nav-link.text-uppercase
    Click Element If Visible    css:.banner-top.container-layout button
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class='iframe-content']/iframe[1]    src
    [Teardown]    Run Keyword When A Step Failed    Virtual Sport${FAILED msg}

Check Sports
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class='iframe-content']/iframe[1][@src]    src
    [Teardown]    Run Keyword When A Step Failed    ${FAILED msg}

Check HomePage
    Click Element If Visible    css:.bg-dark-light.navbar.navbar-dark.navbar-expand-lg \ .brand-logo
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Https    //a[@href]    href
    Check_Response    //img[@src]    src
    [Teardown]    Run Keyword When A Step Failed    HomePage${FAILED msg}

Register Random Account
    Create File    ${path}
    Generate Random Account
    Input Random Register Data
    [Teardown]    Run Keyword When A Step Failed    Register${FAILED msg}

My Open Headless Chrome Browser
    Open Browser    ${HOMEPAGE URL}    headlesschrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080

Generate Random Account
    ${gettime}    Get Current Date    result_format=%b%d%H%M%S@yopmail.com
    Convert To String    ${gettime}
    Set Global Variable    ${gettime}

Input Random Register Data
    Click Element If Visible    css:.gradient-color.register-btn
    Input Text When Element Is Visible    css:form#frm-register \ input[name='username']    ${gettime}
    Input Text When Element Is Visible    css:input#password    123456
    Input Text When Element Is Visible    css:input[name='confirmPassword']    123456
    Input Text When Element Is Visible    css:input[name='phone']    0123456789
    Click Element If Visible    css:form#frm-register \ .btn.btn-primary
    Wait Until Page Contains Element    css:.welcome [href]
