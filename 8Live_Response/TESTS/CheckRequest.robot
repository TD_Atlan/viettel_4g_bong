*** Settings ***
Resource          ../GLOBAL/Pages.robot

*** Test Cases ***
TC00_Register
    Register Random Account

TC01_HomePage
    [Setup]    Go To Home Page
    Check HomePage
    [Teardown]

TC02_Deposit_Page
    [Setup]    Go To Home Page
    Check_Response    //img[@src]    src
    Check_Response_For_Https    //a[@href]    href
    [Teardown]

TC03_Sports
    [Setup]    Go To Home Page
    Check Sports
    [Teardown]

TC04_Casino
    [Setup]    Go To Home Page
    [Template]
    Check Casino
    [Teardown]

TC05_Keno
    [Setup]    Go To Home Page
    [Template]
    Check Keno
    [Teardown]

TC06_NumberGame
    [Setup]    Go To Home Page
    [Template]
    Check NumberGame
    [Teardown]

TC07_SlotsGame
    [Setup]    Go To Home Page
    [Template]
    Check SlotsGame
    [Teardown]

TC08_Lottery
    [Setup]    Go To Home Page
    Check Lottery
    [Teardown]

TC09_News
    [Setup]    Go To Home Page
    [Template]
    Check News
    [Teardown]

TC10_Promotion
    [Setup]    Go To Home Page
    [Template]
    Check Promotion
    [Teardown]
