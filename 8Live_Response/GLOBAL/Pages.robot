*** Settings ***
Library           RPA.Browser    run_on_failure=Nothing    timeout=30    implicit_wait=10
Resource          Locators.robot
Resource          Template.robot
Library           DateTime
Library           String
Library           OperatingSystem
Library           RPA.HTTP

*** Keywords ***
Go To Home Page
    Open Browser    ${HOMEPAGE URL}    headlesschrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080
    Login With Valid Data
    Wait For Condition    return document.readyState=='complete'

Append Content To File
    [Arguments]    ${content}
    Append To File    ${path}    ${content}    encoding=UTF-8

Run Keyword When A Step Failed
    [Arguments]    ${content}
    Run Keyword If    '${KEYWORD STATUS}' == 'FAIL'    Run Keywords    Append Content To File    FAILED\n${content}
    ...    AND    Fail
    Close All Browsers

Login With Valid Data
    Click Element If Visible    css:.btn.btn--login.nav-list__btn
    Input Text When Element Is Visible    css:input[name='username']    Jun30185151
    Input Text When Element Is Visible    css:input[name='password']    123456
    Click Element If Visible    css:.btn.btn--sky-2
    Sleep    3
    [Teardown]

Check_Response_For_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${base_url}    Evaluate    "https" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${resp}    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check Promotion
    Click Element If Visible    css:li:nth-of-type(9) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class='news']//article[@class='bo-content__content']//img    src
    [Teardown]    Run Keyword When A Step Failed    Promotion\nOh! đã xảy ra lỗi\r\n

Check News
    Click Element If Visible    css:li:nth-of-type(8) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@src]    src
    Check_Response_For_Https    //a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    News\nOh! đã xảy ra lỗi\r\n

Check Lottery
    Click Element If Visible    css:li:nth-of-type(6) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@class='card__img']    src
    Check_Response    //img[@class='section__header-img']    src
    [Teardown]    Run Keyword When A Step Failed    Lottery\nOh! đã xảy ra lỗi\r\n

Check SlotsGame
    Click Element If Visible    css:li:nth-of-type(5) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@class='card__img']    src
    Check_Response    //img[@class='section__header-img']    src
    [Teardown]    Run Keyword When A Step Failed    SlotsGame\nOh! đã xảy ra lỗi\r\n

Check NumberGame
    Click Element If Visible    css:li:nth-of-type(4) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@class='card__img']    src
    Check_Response    //img[@class='section__header-img']    src
    [Teardown]    Run Keyword When A Step Failed    NumberGame\nOh! đã xảy ra lỗi\r\n

Check Keno
    Click Element If Visible    css:li:nth-of-type(3) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@class='card__img']    src
    Check_Response    //img[@class='section__header-img']    src
    [Teardown]    Run Keyword When A Step Failed    Keno\nOh! đã xảy ra lỗi\r\n

Check Casino
    Click Element If Visible    css:li:nth-of-type(2) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@class='card__img']    src
    Check_Response    //img[@class='section__header-img']    src
    [Teardown]    Run Keyword When A Step Failed    Casino\nOh! đã xảy ra lỗi\r\n

Check Sports
    Click Element If Visible    css:li:nth-of-type(1) > .menu__link
    Wait For Condition    return document.readyState=='complete'
    Wait Until Page Contains Element    css:div:nth-of-type(1) > div:nth-of-type(2) > .league > .leagueName
    Check_Response    //img[@class='section__header-img']    src
    Check_Response    //img[@src]    src
    [Teardown]    Run Keyword When A Step Failed    Sport\nOh! đã xảy ra lỗi\r\n

Check HomePage
    Click Element If Visible    css:img[alt='logo']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //img[@src]    src
    Check_Response_For_Https    //a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    HomePage\nOh! đã xảy ra lỗi\r\n

Register Random Account
    Open Browser    ${HOMEPAGE URL}    headlesschrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080
    Create File    ${path}
    Run Keyword And Return Status    Wait Until Page Contains Element    css:.btn.btn--register.nav-list__btn
    Click Element If Visible    css:.btn.btn--register.nav-list__btn
    ${gettime}    Get Current Date    result_format=%b%d%H%M%S
    Convert To String    ${gettime}
    Set Global Variable    ${gettime}
    Input Text When Element Is Visible    css:input#input_username    ${gettime}
    Input Text When Element Is Visible    css:input#input_password    123456
    Input Text When Element Is Visible    css:input#input_reinputpassword    123456
    Input Text When Element Is Visible    css:input#input_fullname    ciodsghj
    ${getemail}    Get Current Date    result_format=%b%d%H%M%S@yandex.com
    Convert To String    ${getemail}
    Set Global Variable    ${getemail}
    Input Text When Element Is Visible    css:input#input_email    ${getemail}
    Input Text When Element Is Visible    css:input#input_phone    0123456789
    Click Element If Visible    css:button[name='dangky']
    Sleep    3
    Wait Until Page Contains Element    css:.account__title
    [Teardown]    Run Keyword When A Step Failed    Register\nOh! đã xảy ra lỗi\r\n
